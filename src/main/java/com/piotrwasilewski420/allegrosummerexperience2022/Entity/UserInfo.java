package com.piotrwasilewski420.allegrosummerexperience2022.Entity;

import lombok.Data;

@Data
public class UserInfo {
    private String login;
    private String name;
    private String bio;
}
